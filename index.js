const express = require('express')
const app = express()
const port = 3000

app.use(express.urlencoded({extended:false}))
app.set('view engine', 'ejs')

app.use(express.static('public'))
app.get('/', function(request, response){
    response.render('home');
});

app.get('/rockpaperscissors', (request, response) => {
    response.render('rockpaperscissor');
});

app.get('/loginfailed', (request, response) => {
    response.render('loginfailed')
});

const authRouter = require('./routes/auth.js')
app.use("/auth", authRouter)

app.use((err, req, res, next) => {
    res.status(500).json({
        status : "fail",
        errors : "err.message"
    })    
})

app.use((req, res, next) => {
    res.status(404).json({
        status : "fail",
        errors : "(404) not found"
    })    
})

app.listen(port, () => {
    console.log(`Web started at port : ${port}`)
})