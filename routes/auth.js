const express = require('express')
const router = express.Router()


const users =[]


router.use((request, response, next) => {
    next()
})
router.get('/login', (request, response) => {
    response.render('login')
})

router.post('/login', (req, res) => {

    const { email, password } = req.body
    let user = users.find(i => i.email == email)
    if(user==undefined) {
        console.log('email salah');
        res.render('loginfailed.ejs', {status:'Wrong Email'});
    } else if (user.password!=password) {
        console.log('password salah');
        res.render('loginfailed.ejs', {status:'Wrong Password'});
    } else if (user.password==password) {
        console.log('login berhasil')
        res.redirect('/rockpaperscissors')
    }
})

router.get('/register', (request, response) => {
    response.render('register')
})


router.post('/register', (request, response) => {
    const {email, password} = request.body
    
    users.push({email, password})

    console.log(users)

    response.redirect('/auth/login')
})

module.exports = router
